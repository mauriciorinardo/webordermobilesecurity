# Web Order Mobile Security

This is a modified version of DataFlex 19.1 Web Order Mobile sample workspace with security related modifications that can be used by DataFlex developers in ther own web applications.

This is a work in progress and any contribution is welcome.

##Current Features

1. Passcode storage with Argon2id using the DataFlex Security Library
2. Password reset through reset link sent to user's e-mail address (e-mail function not available)
3. Password change screen with password policy enforcements (minimum length, character requirements, password reuse, pwned password checking)
4. Second factor authentication (TOTP). Missing Login part will be added soon.
5. 2 SQLi sample views

##Contact Information

Jo�o Maur�cio Rinardo
joao-r at DataAccess.com.br